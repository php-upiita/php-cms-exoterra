<?php
  require_once 'General.php';

  class CtrlNewAnimal extends General {
    protected $idAnimal;

    public function __construct(){
      if(isset($_SESSION['idUsuario'])){
        //Create new state in DB
        if($this->newAnimal($_POST)){
          //Upload img to server
          if($this->uploadImg($_FILES)){
            $_SESSION['alerta'] = 'El animal fue creado con éxito';
            $_SESSION['tipoAlerta'] = 'success';  
            header("Location: ../dashboard.php");
          }else{
            $_SESSION['alerta'] = 'No se pudieron subir las imágenes';
            $_SESSION['tipoAlerta'] = 'danger';  
            header("Location: ../dashboard.php"); 
            exit();
          } //End upload IMG to server
        }else{
          $_SESSION['alerta'] = 'El animal no pudo ser creado';
          $_SESSION['tipoAlerta'] = 'danger';  
          header("Location: ../dashboard.php");   
        } //End creation new state in DB
      }else{
        $_SESSION['alerta'] = 'Por favor inicia sesión';
        $_SESSION['tipoAlerta'] = 'danger';  
        header("Location: ../index.php");
        exit();
      }

    }

    private function newAnimal($_data){
      try{
        if($this->conectaBd()){
          $query = "INSERT INTO
                    animals(
                      name,
                      description,
                      create_at)
                    VALUES(
                      :name,
                      :description,
                      NOW())";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindValue(':name', trim($_data['inputName']), PDO::PARAM_STR);
          $cmd->bindValue(':description', trim($_data['description']), PDO::PARAM_STR);
          if($cmd->execute()){
            $this->idAnimal = $this->cnxBd->lastInsertId();
            return true;
          }else{
            echo 'Algo salio mal';
            die();
          }
        }else{
          echo '{"Error": 05}';
          die();  
        }
      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

    private function uploadImg($_files){
      $count=0;
      $flag = true;
      $server = "../files/animals/";
      $target = $server.$this->idAnimal.'/';

      if(!file_exists($target)){
        if (!mkdir($target, 0755, true)) {
          $_SESSION['alerta'] = 'animal registrado sin imágenes';
          $_SESSION['tipoAlerta'] = 'warning';
          header("Location: ../dashboard.php");
        }//End mkdir
      }

      
      $imgTmp =  $_files['img']['tmp_name'];
      $imgName =  basename($_files["img"]["name"]);
      $pdfTmp =  $_files['pdf']['tmp_name'];
      $pdfName =  basename($_files["pdf"]["name"]);

      if (move_uploaded_file($imgTmp, $target.$imgName) && move_uploaded_file($pdfTmp, $target.$pdfName) ) {
      }else{
        $flag = false;
      }
      if ($flag) {
        if ($this->setImg($this->idAnimal, $imgName, $pdfName)) {
          return true;
        }else {
          return false;
        }
      }else {
        return false;
      }
      
    } //En function


    public function setImg($_idAnimal, $_imgName, $_pdfName){
      try{
        if($this->conectaBd()){
          $query = "UPDATE animals
                    SET
                      picture = :picture,
                      pdf_link = :pdf_link
                    WHERE id_animal = :idAnimal;";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindValue(':picture', trim($_idAnimal.'/'.$_imgName), PDO::PARAM_STR);
          $cmd->bindValue(':pdf_link', trim($_idAnimal.'/'.$_pdfName), PDO::PARAM_STR);
          $cmd->bindValue(':idAnimal', trim($_idAnimal), PDO::PARAM_INT);
          if($cmd->execute()){
            return true;
          }else{
            return false;
            die();
          }
        }else{
          echo '{"Error": 05}';
          die();  
        }
      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

  }
