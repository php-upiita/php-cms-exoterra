<?php

require_once 'General.php';

class CtrlAnimal extends General {

  public $animal;
  
  public function __construct() {
    /** Procesar peticiones **/
    if(isset($_SESSION['idUsuario'])){
      if(isset($_GET['pid'])){
        if((int)$_GET['pid'] != 0){
          $this->getAnimal($_GET['pid']);
        }else{
          die('Intento de contaminar base de datos');
        }
      }else{
        die('Debes editar un animal');
      }
    }else{
      header("Location: index.php");
      exit();
    }
  }

  private function getAnimal($_idAnimal){
    try {
      if ($this->conectaBd()){
        $query = "SELECT
                  id_animal,
                  name,
                  description,
                  picture,
                  pdf_link
                FROM animals
                WHERE id_animal = :idAnimal;";
        $cmd = $this->cnxBd->prepare($query);
        $cmd->bindParam(':idAnimal', $_idAnimal, PDO::PARAM_INT);
        $cmd->execute();
        $res = $cmd->fetchObject();
        if(isset($res->id_animal)){
          $this->animal = $res;
        }                 
      } else {
        echo '{"Error": 05}';
        die();
      }
    } catch (Exception $ex) {
      echo "Exception -> ";
      var_dump($ex->getMessage());
    }
  }


}
  
