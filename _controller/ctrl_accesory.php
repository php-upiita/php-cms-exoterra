<?php

require_once 'General.php';

class CtrlAccesory extends General {

  public $accesory;
  
  public function __construct() {
    /** Procesar peticiones **/
    if(isset($_SESSION['idUsuario'])){
      if(isset($_GET['pid'])){
        if((int)$_GET['pid'] != 0){
          $this->getAnimal($_GET['pid']);
        }else{
          die('Intento de contaminar base de datos');
        }
      }else{
        die('Debes editar un animal');
      }
    }else{
      header("Location: index.php");
      exit();
    }
  }

  private function getAnimal($_idAccesory){
    try {
      if ($this->conectaBd()){
        $query = "SELECT
                  id_accesory,
                  name,
                  description,
                  picture,
                  price
                FROM accesories
                WHERE id_accesory = :idAccesory;";
        $cmd = $this->cnxBd->prepare($query);
        $cmd->bindParam(':idAccesory', $_idAccesory, PDO::PARAM_INT);
        $cmd->execute();
        $res = $cmd->fetchObject();
        if(isset($res->id_accesory)){
          $this->accesory = $res;
        }                 
      } else {
        echo '{"Error": 05}';
        die();
      }
    } catch (Exception $ex) {
      echo "Exception -> ";
      var_dump($ex->getMessage());
    }
  }


}
  
