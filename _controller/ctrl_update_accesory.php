<?php
  require_once 'General.php';

  class CtrlUpdateAccesory extends General {
    protected $idAccesory;

    public function __construct(){
      if(isset($_SESSION['idUsuario'])){
        //Create new state in DB
        if($this->updateAccesory($_POST)){
          $this->idAccesory = $_POST['idAccesory'];
          //Upload img to server
          if($this->uploadImg($_FILES)){
            $_SESSION['alerta'] = 'El accesorio fue actualizado con éxito';
            $_SESSION['tipoAlerta'] = 'success';  
            header("Location: ../accesorios.php");
          }else{
            $_SESSION['alerta'] = 'No se pudieron subir las imágenes';
            $_SESSION['tipoAlerta'] = 'danger';  
            header("Location: ../accesorios.php"); 
            exit();
          } //End upload IMG to server
        }else{

          $_SESSION['alerta'] = 'El accesorio no pudo ser actualizado';
          $_SESSION['tipoAlerta'] = 'danger';  
          header("Location: ../accesorios.php");   
        }
      }else{
        $_SESSION['alerta'] = 'Por favor inicia sesión';
        $_SESSION['tipoAlerta'] = 'danger';  
        header("Location: ../index.php");
        exit();
      }

    }

    private function updateAccesory($_data){
      try{
        if($this->conectaBd()){
          $query = "UPDATE accesories
                    SET
                      name = :name,
                      description = :description,
                      price = :price
                    WHERE id_accesory = :idAccesory;";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindValue(':name', trim($_data['inputName']), PDO::PARAM_STR);
          $cmd->bindValue(':description', trim($_data['description']), PDO::PARAM_STR);
          $cmd->bindValue(':price', trim($_data['price']), PDO::PARAM_INT);
          $cmd->bindValue(':idAccesory', trim($_data['idAccesory']), PDO::PARAM_INT);
          if($cmd->execute()){
            return true;
          }else{
            echo 'Algo salio mal';
            die();
          }
        }else{
          echo '{"Error": 05}';
          die();  
        }
      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

    private function uploadImg($_files){     
      $count=0;
      $flag = true;
      $server = "../files/accesories/";
      $target = $server.$this->idAccesory.'/';

      if(!file_exists($target)){
        if (!mkdir($target, 0755, true)) {
          $_SESSION['alerta'] = 'animal registrado sin imágenes';
          $_SESSION['tipoAlerta'] = 'warning';
          header("Location: ../accesorios.php");
        }//End mkdir
      }

      if($_files["img"]["name"] != ""){
        $imgTmp =  $_files['img']['tmp_name'];
        $imgName =  basename($_files["img"]["name"]);

        if (move_uploaded_file($imgTmp, $target.$imgName) ) {
        }else{
          $flag = false;
        }

        if ($flag) {
          if ($this->setImg($this->idAccesory, $imgName)) {
            $flag = true;
          }else {
            $flag = false;
          }
        }else {
            $flag = false;
        }
      }


      return $flag;

      
    } //En function


    public function setImg($_idAccesory, $_imgName){
      try{
        if($this->conectaBd()){
          $query = "UPDATE accesories
                    SET
                      picture = :picture
                    WHERE id_accesory = :idAccesory;";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindValue(':picture', trim($_idAccesory.'/'.$_imgName), PDO::PARAM_STR);
          $cmd->bindValue(':idAccesory', trim($_idAccesory), PDO::PARAM_INT);
          if($cmd->execute()){
            return true;
          }else{
            return false;
            die();
          }
        }else{
          echo '{"Error": 05}';
          die();  
        }
      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

   


  }
