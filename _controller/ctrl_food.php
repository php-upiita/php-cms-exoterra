<?php

require_once 'General.php';

class CtrlFood extends General {

  public $food;
  
  public function __construct() {
    /** Procesar peticiones **/
    if(isset($_SESSION['idUsuario'])){
      if(isset($_GET['pid'])){
        if((int)$_GET['pid'] != 0){
          $this->getAnimal($_GET['pid']);
        }else{
          die('Intento de contaminar base de datos');
        }
      }else{
        die('Debes editar un animal');
      }
    }else{
      header("Location: index.php");
      exit();
    }
  }

  private function getAnimal($_idFood){
    try {
      if ($this->conectaBd()){
        $query = "SELECT
                  id_food,
                  name,
                  description,
                  picture,
                  price
                FROM foods
                WHERE id_food = :idFood;";
        $cmd = $this->cnxBd->prepare($query);
        $cmd->bindParam(':idFood', $_idFood, PDO::PARAM_INT);
        $cmd->execute();
        $res = $cmd->fetchObject();
        if(isset($res->id_food)){
          $this->food = $res;
        }                 
      } else {
        echo '{"Error": 05}';
        die();
      }
    } catch (Exception $ex) {
      echo "Exception -> ";
      var_dump($ex->getMessage());
    }
  }


}
  
