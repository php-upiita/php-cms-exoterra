<?php
  require_once 'General.php';

  class CtrlDashboard extends General {
    public $animals;
    
    public function __construct(){
      if(isset($_SESSION['idUsuario'])){
        $this->getStates();
      }else{
        header("Location: index.php");
        $_SESSION['tipoAlerta'] = "warning";
        $_SESSION['alerta'] = "Tu sessión ha expirado, vuelve a iniciar sesión";
        exit();
      }
    }

    public function getStates(){
      try{
          if($this->conectaBd()){
            $query = "SELECT
                        id_animal as id,
                        name as nombre,
                        description as descripcion,
                        picture as imagen,
                        pdf_link as pdf_enlace,
                        create_at as creado_el
                      FROM animals
                      ORDER BY nombre DESC LIMIT 500;";
              $cmd = $this->cnxBd->prepare($query);
              $cmd->execute();
              $res = $cmd->fetchAll(PDO::FETCH_ASSOC);
              $this->animals = json_encode($res);
          }else{
            echo json_encode(array(
              'error' => array(
                  'code' => 05,
                  'message' => 'No pudimos establecer conexión con la BD'
              )
          ));
          }

      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

  }
