<?php
  require_once 'General.php';

  class CtrlComidas extends General {
    public $foods;
    
    public function __construct(){
      if(isset($_SESSION['idUsuario'])){
        $this->getStates();
      }else{
        header("Location: index.php");
        $_SESSION['tipoAlerta'] = "warning";
        $_SESSION['alerta'] = "Tu sessión ha expirado, vuelve a iniciar sesión";
        exit();
      }
    }

    public function getStates(){
      try{
          if($this->conectaBd()){
            $query = "SELECT
                        id_food as id,
                        name as nombre,
                        description as descripcion,
                        picture as imagen,
                        price as precio,
                        created_at as creado_el
                      FROM foods
                      ORDER BY nombre DESC LIMIT 500;";
              $cmd = $this->cnxBd->prepare($query);
              $cmd->execute();
              $res = $cmd->fetchAll(PDO::FETCH_ASSOC);
              $this->foods = json_encode($res);
          }else{
            echo json_encode(array(
              'error' => array(
                  'code' => 05,
                  'message' => 'No pudimos establecer conexión con la BD'
              )
          ));
          }

      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

  }
