<?php
require_once 'General.php';
class CtrlLogin extends General {
  public function __construct() {
    /** Procesar peticiones **/
    if(isset($_SESSION['user'])){
      header("Location: dashboard.php");
    }else{
      if(isset($_POST['username']) && isset($_POST['password'])){
        $this->iniciarSesion($_POST['username'],$_POST['password']);
      }else{
        $_SESSION['tipoAlerta'] = "danger";
        $_SESSION['alerta'] = "Llene todos los campos por favor.";
        header("Location: index.php");
      }
    }
  }

  private function iniciarSesion($_email,$_password) {
    try {
      if ($this->conectaBd()){
        $query = "SELECT id_user, email, name, lastname,password FROM users WHERE email = :email;";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindParam(':email', $_email, PDO::PARAM_STR);
          $cmd->execute();
          $res = $cmd->fetchObject();
          if(password_verify($_password, $res->password)){
            session_start();
            $_SESSION['idUsuario'] = $res->id_user;
            $_SESSION['nombre'] = $res->name.' '.$res->lastname;
            header("Location: dashboard.php");
          }else{
            $_SESSION['tipoAlerta'] = "danger";
            $_SESSION['alerta'] = "Los datos no coinciden, intente de nuevo por favor.";
            header("Location: index.php");
          }                  
      }else {
        echo '{"Error": 05}';
        die();
      }
    } catch (Exception $ex) {
      echo "Exception -> ";
			var_dump($ex->getMessage());
		}
  }
}
