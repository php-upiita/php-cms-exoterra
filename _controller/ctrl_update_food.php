<?php
  require_once 'General.php';

  class CtrlUpdateFood extends General {
    protected $idFood;

    public function __construct(){
      if(isset($_SESSION['idUsuario'])){
        //Create new state in DB
        if($this->updateFood($_POST)){
          $this->idFood = $_POST['idFood'];
          //Upload img to server
          if($this->uploadImg($_FILES)){
            $_SESSION['alerta'] = 'El animal fue actualizado con éxito';
            $_SESSION['tipoAlerta'] = 'success';  
            header("Location: ../comidas.php");
          }else{
            $_SESSION['alerta'] = 'No se pudieron subir las imágenes';
            $_SESSION['tipoAlerta'] = 'danger';  
            header("Location: ../comidas.php"); 
            exit();
          } //End upload IMG to server
        }else{

          $_SESSION['alerta'] = 'El animal no pudo ser actualizado';
          $_SESSION['tipoAlerta'] = 'danger';  
          header("Location: ../comidas.php");   
        }
      }else{
        $_SESSION['alerta'] = 'Por favor inicia sesión';
        $_SESSION['tipoAlerta'] = 'danger';  
        header("Location: ../index.php");
        exit();
      }

    }

    private function updateFood($_data){
      try{
        if($this->conectaBd()){
          $query = "UPDATE foods
                    SET
                      name = :name,
                      description = :description,
                      price = :price
                    WHERE id_food = :idFood;";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindValue(':name', trim($_data['inputName']), PDO::PARAM_STR);
          $cmd->bindValue(':description', trim($_data['description']), PDO::PARAM_STR);
          $cmd->bindValue(':price', trim($_data['price']), PDO::PARAM_INT);
          $cmd->bindValue(':idFood', trim($_data['idFood']), PDO::PARAM_INT);
          if($cmd->execute()){
            return true;
          }else{
            echo 'Algo salio mal';
            die();
          }
        }else{
          echo '{"Error": 05}';
          die();  
        }
      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

    private function uploadImg($_files){

      

     
      $count=0;
      $flag = true;
      $server = "../files/foods/";
      $target = $server.$this->idFood.'/';

      if(!file_exists($target)){
        if (!mkdir($target, 0755, true)) {
          $_SESSION['alerta'] = 'animal registrado sin imágenes';
          $_SESSION['tipoAlerta'] = 'warning';
          header("Location: ../comidas.php");
        }//End mkdir
      }

      if($_files["img"]["name"] != ""){
        $imgTmp =  $_files['img']['tmp_name'];
        $imgName =  basename($_files["img"]["name"]);

        if (move_uploaded_file($imgTmp, $target.$imgName) ) {
        }else{
          $flag = false;
        }

        if ($flag) {
          if ($this->setImg($this->idFood, $imgName)) {
            $flag = true;
          }else {
            $flag = false;
          }
        }else {
            $flag = false;
        }
      }


      return $flag;

      
    } //En function


    public function setImg($_idFood, $_imgName){
      try{
        if($this->conectaBd()){
          $query = "UPDATE foods
                    SET
                      picture = :picture
                    WHERE id_food = :idFood;";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindValue(':picture', trim($_idFood.'/'.$_imgName), PDO::PARAM_STR);
          $cmd->bindValue(':idFood', trim($_idFood), PDO::PARAM_INT);
          if($cmd->execute()){
            return true;
          }else{
            return false;
            die();
          }
        }else{
          echo '{"Error": 05}';
          die();  
        }
      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

   


  }
