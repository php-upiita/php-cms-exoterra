<?php
  require_once 'General.php';

  class CtrlNewAccesory extends General {
    protected $idAccesory;

    public function __construct(){
      if(isset($_SESSION['idUsuario'])){
        //Create new state in DB
        if($this->newAccesory($_POST)){
          //Upload img to server
          if($this->uploadImg($_FILES)){
            $_SESSION['alerta'] = 'El animal fue creado con éxito';
            $_SESSION['tipoAlerta'] = 'success';  
            header("Location: ../accesorios.php");
          }else{
            $_SESSION['alerta'] = 'No se pudieron subir las imágenes';
            $_SESSION['tipoAlerta'] = 'danger';  
            header("Location: ../accesorios.php"); 
            exit();
          } //End upload IMG to server
        }else{
          $_SESSION['alerta'] = 'El animal no pudo ser creado';
          $_SESSION['tipoAlerta'] = 'danger';  
          header("Location: ../accesorios.php");   
        } //End creation new state in DB
      }else{
        $_SESSION['alerta'] = 'Por favor inicia sesión';
        $_SESSION['tipoAlerta'] = 'danger';  
        header("Location: ../index.php");
        exit();
      }

    }

    private function newAccesory($_data){
      try{
        if($this->conectaBd()){
          $query = "INSERT INTO
                    accesories(
                      name,
                      description,
                      price,
                      created_at)
                    VALUES(
                      :name,
                      :description,
                      :price,
                      NOW())";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindValue(':name', trim($_data['inputName']), PDO::PARAM_STR);
          $cmd->bindValue(':description', trim($_data['description']), PDO::PARAM_STR);
          $cmd->bindValue(':price', trim($_data['price']), PDO::PARAM_INT);
          if($cmd->execute()){
            $this->idAccesory = $this->cnxBd->lastInsertId();
            return true;
          }else{
            echo 'Algo salio mal';
            die();
          }
        }else{
          echo '{"Error": 05}';
          die();  
        }
      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

    private function uploadImg($_files){
      $count=0;
      $flag = true;
      $server = "../files/accesories/";
      $target = $server.$this->idAccesory.'/';

      if(!file_exists($target)){
        if (!mkdir($target, 0755, true)) {
          $_SESSION['alerta'] = 'animal registrado sin imágenes';
          $_SESSION['tipoAlerta'] = 'warning';
          header("Location: ../accesorios.php");
        }//End mkdir
      }

      
      $imgTmp =  $_files['img']['tmp_name'];
      $imgName =  basename($_files["img"]["name"]);


      if (move_uploaded_file($imgTmp, $target.$imgName)) {
      }else{
        $flag = false;
      }
      if ($flag) {
        if ($this->setImg($this->idAccesory, $imgName)) {
          return true;
        }else {
          return false;
        }
      }else {
        return false;
      }
      
    } //En function


    public function setImg($_idAccesory, $_imgName){
      try{
        if($this->conectaBd()){
          $query = "UPDATE accesories
                    SET
                      picture = :picture
                    WHERE id_accesory = :idAccesory;";
          $cmd = $this->cnxBd->prepare($query);
          $cmd->bindValue(':picture', trim($_idAccesory.'/'.$_imgName), PDO::PARAM_STR);
          $cmd->bindValue(':idAccesory', trim($_idAccesory), PDO::PARAM_INT);
          if($cmd->execute()){
            return true;
          }else{
            return false;
            die();
          }
        }else{
          echo '{"Error": 05}';
          die();  
        }
      }catch(Exception $ex){
        echo json_encode(array(
          'error' => array(
              'code' => $ex->getCode(),
              'message' => $ex->getMessage()
          )
      ));
      }

    }

  }
