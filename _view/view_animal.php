<div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form action="_post/updateAnimal.php"  method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <h2>Guardar Animal</h2>
            </div>
            <div class="col-md-6 text-right">
              <input type="submit" class="btn btn-success" value="Guardar">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="box">
                <div class="box-header">
                  <h4>Datos del animal</h4>
                <div class="box-body">
                  <input type="text" name="idAnimal" id="idAnimal" value="<?php echo $ctrl->animal->id_animal; ?>" hidden>
                  <div class="form-group">
                    <label for="inputName">Nombre del animal</label>
                    <input type="text" class="form-control  " id="inputName" name="inputName" placeholder="Ingresa el nombre" value="<?php echo $ctrl->animal->name; ?>" required>
                    <div class="invalid-feedback"> Por favor ingresa un nombre </div>
                  </div>
                  <div class="form-group">
                    <label for="description">Descripción del animal</label>
                    <textarea type="text" class="form-control" id="editor" name="description" rows="10"><?php echo $ctrl->animal->description; ?></textarea>
                    <script>
                      tinymce.init({
                        selector: 'textarea',
                        plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                        toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
                        toolbar_mode: 'floating',
                        tinycomments_mode: 'embedded',
                        tinycomments_author: 'Author name',
                      });
                    </script>
                    <div class="invalid-feedback"> Por favor ingresa una descripción </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="box">
                <div class="box-header">
                  <h4>Datos de creación</h4>
                </div>
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputImgs">Ingresa la imagen del animal</label>
                    <input type='file' name='img' id="inputImgs" accept=".png, .jpg, .jpeg">
                  </div>                  
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-4">
                        <img class="img-fluid" src="<?php echo 'files/animals/'.$ctrl->animal->picture?>" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputImgs">Ingresa el pdf del animal</label>
                    <input type='file' name='pdf' id="inputPdf" accept=".pdf">
                  </div>                  
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-4">
                        <a href="<?php echo 'files/animals/'.$ctrl->animal->pdf_link?>" target="_blank">Ver pdf</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
</div>
