<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <h1> Accesorio </h1>
    </div>
    <div class="col-md-6"></div>
    <div class="col-md-2 mt-2">
      <a class="btn btn-success" href="create-accesory.php" role="button">Nuevo Accesorio</a>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <?php
        if(isset($_SESSION['alerta'])){
      ?>
      <div class="alert alert-<?php echo $_SESSION['tipoAlerta'];?> alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong><?php echo $_SESSION['alerta']; ?></strong>
      </div>
      <?php 
      unset($_SESSION['tipoAlerta']);
      unset($_SESSION['alerta']);
        }
      ?>
    </div>
    <div class="col-md-12">
      <!-- ***************** Table start***************** -->
      <div class="table-responsive">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nombre del accesorio</th>
              <th scope="col">Descripción</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $accesorios = json_decode($ctrl->accesories); 
              foreach ($accesorios as $accesorio){
            ?>
            <tr>
              <td><?php echo $accesorio->id;?></td>
              <td> <a href='accesory.php?pid=<?php echo $accesorio->id; ?>'> <?php echo $accesorio->nombre;?> </a></td>
              <td><?php echo substr($accesorio->descripcion,0,180);?></td>
            </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
