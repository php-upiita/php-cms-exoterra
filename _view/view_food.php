<div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form action="_post/updateFood.php"  method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <h2>Guardar Alimento</h2>
            </div>
            <div class="col-md-6 text-right">
              <input type="submit" class="btn btn-success" value="Guardar">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="box">
                <div class="box-header">
                  <h4>Datos del alimento</h4>
                <div class="box-body">
                <input type="text" name="idFood" id="idFood" value="<?php echo $ctrl->food->id_food; ?>" hidden>
                <div class="form-group">
                    <label for="inputName">Nombre del alimento</label>
                    <input type="text" class="form-control  " id="inputName" name="inputName" placeholder="Ingresa el nombre" value="<?php echo $ctrl->food->name; ?>" required>
                    <div class="invalid-feedback"> Por favor ingresa un nombre </div>
                  </div>
                  <div class="form-group">
                    <label for="description">Descripción del alimento</label>
                    <textarea type="text" class="form-control" id="editor" name="description" rows="10"><?php echo $ctrl->food->description; ?></textarea>
                    <script>
                      tinymce.init({
                        selector: 'textarea',
                        plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                        toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
                        toolbar_mode: 'floating',
                        tinycomments_mode: 'embedded',
                        tinycomments_author: 'Author name',
                      });
                    </script>                    
                    <div class="invalid-feedback"> Por favor ingresa una descripción </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="box">
                <div class="box-header">
                  <h4>Datos de creación</h4>
                </div>
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputImgs">Ingresa la imagen del alimento</label>
                    <input type='file' name='img' id="inputImgs" accept=".png, .jpg, .jpeg">
                  </div>
                  <div class="form-group">
                    <label for="price">Precio del alimento</label>
                    <input type="number" class="form-control" id="price" name="price" min="1" step="any" value="<?php echo $ctrl->food->price?>" required>
                    <div class="invalid-feedback"> Por favor ingresa un precio </div>
                  </div>                  
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-4">
                        <img class="img-fluid" src="<?php echo 'files/foods/'.$ctrl->food->picture?>" alt="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
</div>
