<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <h1> Animales </h1>
    </div>
    <div class="col-md-6"></div>
    <div class="col-md-2 mt-2">
      <a class="btn btn-success" href="create-animal.php" role="button">Nuevo animal</a>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <?php
        if(isset($_SESSION['alerta'])){
      ?>
      <div class="alert alert-<?php echo $_SESSION['tipoAlerta'];?> alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong><?php echo $_SESSION['alerta']; ?></strong>
      </div>
      <?php 
      unset($_SESSION['tipoAlerta']);
      unset($_SESSION['alerta']);
        }
      ?>
    </div>
    <div class="col-md-12">
      <!-- ***************** Table start***************** -->
      <div class="table-responsive">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nombre del animal</th>
              <th scope="col">Descripción</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $animales = json_decode($ctrl->animals); 
              foreach ($animales as $animal){
            ?>
            <tr>
              <td><?php echo $animal->id;?></td>
              <td> <a href='animal.php?pid=<?php echo $animal->id; ?>'> <?php echo $animal->nombre;?> </a></td>
              <td><?php echo substr($animal->descripcion, 0, 180);?></td>
            </tr>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
