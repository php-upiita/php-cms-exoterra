<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/main.css">
    <script src="https://cdn.tiny.cloud/1/zsdln046prp6znpvbu2pmlx2qt5dexj0aotej4tvoiyfpnjp/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  </head>
  <body>
    <div class="d-flex" id="wrapper">
      <!-- Sidebar -->
      <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">Bienvenido <?php echo $_SESSION['nombre'];?> </div>
        <div class="list-group list-group-flush">
          <a href="dashboard.php" class="list-group-item list-group-item-action bg-light">Dashboard</a>
          <a href="comidas.php" class="list-group-item list-group-item-action bg-light">Comidas</a>
          <a href="accesorios.php" class="list-group-item list-group-item-action bg-light">Accesorios</a>
          <a href="logout.php" class="list-group-item list-group-item-action bg-light">Cerrar sesión</a>
        </div>
      </div>
      <!-- /#sidebar-wrapper -->
      <!-- Page Content -->
      <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
              <li class="nav-item">
                <a class="nav-link" ><?php echo $_SESSION['nombre']?></a>
              </li>
            </ul>
          </div>
        </nav>

