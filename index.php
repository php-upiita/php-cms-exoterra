<?php session_start(); 
  if(isset($_SESSION['user'])){
    header('Location: dashboard.php');
  }  
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Exoterra-CMS</title>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row text-center mt-5">
        <div class="col-md-12">
          <img src="assets/img/logo.png" alt="azumar-logo" class="logo-img w-25">
        </div>
        <div class="col-md-12 mt-5">
          <h1>Iniciar sesión</h1>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
        <?php
            if(isset($_SESSION['alerta'])){
          ?>
          <div class="alert alert-<?php echo $_SESSION['tipoAlerta'];?> alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?php echo $_SESSION['alerta']; ?></strong>
          </div>
          <?php 
          unset($_SESSION['tipoAlerta']);
          unset($_SESSION['alerta']);
            }
          ?>
        <form action="login.php" method="POST" id="signinform">
          <div class="form-group">
            <label for="exampleInputEmail1">Nombre de usuario</label>
            <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="Ingresa tu nombre de usuario">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Contraseña</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="contraseña">
          </div>
          <button type="submit" class="btn btn-primary">Iniciar sesión</button>
        </form>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>